package com.bank.online.bank.dao;

import com.bank.online.bank.domain.PrimaryAccount;
import org.springframework.data.repository.CrudRepository;

public interface PrimaryAccountDao extends CrudRepository<PrimaryAccount,Long>{

    PrimaryAccount findByAccountNumber(Integer accountNumber);
}
