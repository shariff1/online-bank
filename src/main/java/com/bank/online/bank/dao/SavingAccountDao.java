package com.bank.online.bank.dao;

import com.bank.online.bank.domain.SavingAccount;
import org.springframework.data.repository.CrudRepository;

public interface SavingAccountDao extends CrudRepository<SavingAccount, Long> {
    SavingAccount findByAccountNumber(int accountNumber);
}
