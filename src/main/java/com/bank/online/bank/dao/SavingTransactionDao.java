package com.bank.online.bank.dao;

import com.bank.online.bank.domain.SavingTransaction;
import org.springframework.data.repository.CrudRepository;

public interface SavingTransactionDao extends CrudRepository<SavingTransaction,Long>{
}
