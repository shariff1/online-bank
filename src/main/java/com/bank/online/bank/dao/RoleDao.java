package com.bank.online.bank.dao;

import com.bank.online.bank.domain.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleDao extends CrudRepository<Role, Integer> {
     Role findByName(String name);
}
