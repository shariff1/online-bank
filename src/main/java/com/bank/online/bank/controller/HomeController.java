package com.bank.online.bank.controller;

import com.bank.online.bank.dao.RoleDao;
import com.bank.online.bank.domain.PrimaryAccount;
import com.bank.online.bank.domain.SavingAccount;
import com.bank.online.bank.domain.User;
import com.bank.online.bank.domain.UserRole;
import com.bank.online.bank.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

@Controller
public class HomeController {
    @Autowired
    private UserService userService;
    @Autowired
    private RoleDao roleDao;

    @RequestMapping("/")
    public String home() {
        return "redirect:/index";
    }

    @RequestMapping("/index")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "signup";

    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signupPost(@ModelAttribute("user") User user,
                             Model model) {
        if (userService.checkUserExists(user.getUsername(), user.getEmail())) {
            if (userService.checkEmailExists(user.getEmail())) {
                model.addAttribute("email Exists", true);
            }
            if (userService.checkUserNameExists(user.getUsername())) {
                model.addAttribute("UserName Exists", "true");
            }
            return "signp";

        } else {
            Set<UserRole> userRole = new HashSet<>();
            userRole.add(new UserRole(user, roleDao.findByName("ROLE_USER")));
            userService.createUser(user, userRole);
            return "redirect:/";

        }

    }

    @RequestMapping("/userFront")
    public String userFront(Principal principal,
                            Model model) {
        User user = userService.findByUsername(principal.getName());
        PrimaryAccount primaryAccount = user.getPrimaryAccount();
        SavingAccount savingAccount = user.getSavingAccount();
        model.addAttribute("primaryAccount", primaryAccount);
        model.addAttribute("savingsAccount", savingAccount);
        return "userFront";

    }
}
