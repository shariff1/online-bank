package com.bank.online.bank.service;

import com.bank.online.bank.dao.UserDao;
import com.bank.online.bank.domain.User;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserSecurityService implements UserDetailsService {
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger("User Security Service");
    @Autowired
    UserDao userDao;

    @Override

    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userDao.findByUsername(s);
        if (user == null) {
            LOG.info("username not found");
            throw new UsernameNotFoundException("user name not found" + s);
        }
        return user;

    }
}
