package com.bank.online.bank.service;

import com.bank.online.bank.domain.User;
import com.bank.online.bank.domain.UserRole;

import java.util.Set;

public interface UserService {
    boolean checkUserExists(String username,
                            String email);

    boolean checkEmailExists(String email);

    boolean checkUserNameExists(String username);

    User createUser(User user,
                    Set<UserRole> userRole);

    User findByUsername(String name);
}
