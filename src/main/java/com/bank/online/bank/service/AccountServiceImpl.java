package com.bank.online.bank.service;

import com.bank.online.bank.dao.PrimaryAccountDao;
import com.bank.online.bank.dao.SavingAccountDao;
import com.bank.online.bank.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.Date;

@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    PrimaryAccountDao primaryAccountDao;
    @Autowired
    SavingAccountDao savingAccountDao;
    @Autowired
    UserService userService;
    @Autowired
    TransactionService transactionService;

    @Override
    public PrimaryAccount createPrimaryAccount() {
        PrimaryAccount primaryAccount = new PrimaryAccount();
        primaryAccount.setAccountNumber(generateAccounNumber());
        primaryAccount.setAccountBalance(new BigDecimal(0.0));
        primaryAccountDao.save(primaryAccount);
        return primaryAccountDao.findByAccountNumber(primaryAccount.getAccountNumber());
    }

    private Integer generateAccounNumber() {
        Integer number = 98458;
        return ++number;
    }

    @Override
    public SavingAccount createSavingAccount() {
        SavingAccount savingAccount = new SavingAccount();
        savingAccount.setAccountBalance(new BigDecimal(0.0));
        savingAccount.setAccountNumber(generateAccounNumber());
        savingAccountDao.save(savingAccount);
        return savingAccountDao.findByAccountNumber(savingAccount.getAccountNumber());
    }

    @Override
    public void deposit(double amount,
                        String accountType,
                        Principal principal) {
        User user = userService.findByUsername(principal.getName());
        if (accountType.equalsIgnoreCase("Primary")) {
            PrimaryAccount primaryAccount = user.getPrimaryAccount();
            primaryAccount.setAccountBalance(primaryAccount.getAccountBalance().add(new BigDecimal(amount)));
            primaryAccountDao.save(primaryAccount);
            Date date = new Date();
            PrimaryTransaction primaryTransaction = new PrimaryTransaction(date, "deposit to primary account",
                    "Account", "Finished", amount, primaryAccount.getAccountBalance(), primaryAccount);
            transactionService.savePrimaryDepositTransaction(primaryTransaction);
        } else if (accountType.equalsIgnoreCase("Saving")) {
            SavingAccount savingAccount = user.getSavingAccount();
            savingAccount.setAccountBalance(savingAccount.getAccountBalance().add(new BigDecimal(amount)));
            savingAccountDao.save(savingAccount);
            Date date = new Date();
            SavingTransaction savingTransaction = new SavingTransaction(date, "deposit in saving account", "Account",
                    "Finished", amount, savingAccount.getAccountBalance(), savingAccount);
            transactionService.saveSavingDepositTransaction(savingTransaction);

        }
    }

    @Override
    public void withdraw(String accountType,
                         double amount,
                         Principal principal) {
        User user = userService.findByUsername(principal.getName());
        if (accountType.equalsIgnoreCase("Primary")) {
            PrimaryAccount primaryAccount = user.getPrimaryAccount();
            primaryAccount.setAccountBalance(primaryAccount.getAccountBalance().subtract(new BigDecimal(amount)));
            primaryAccountDao.save(primaryAccount);
            Date date= new Date();
            PrimaryTransaction primaryTransaction = new PrimaryTransaction(date, "withdraw to primary account",
                    "Account", "Finished", amount, primaryAccount.getAccountBalance(), primaryAccount);
            transactionService.savePrimaryWithDrawTransaction(primaryTransaction);

        } else if (accountType.equalsIgnoreCase("Primary")) {
            SavingAccount savingAccount = user.getSavingAccount();
            savingAccount.setAccountBalance(savingAccount.getAccountBalance().subtract(new BigDecimal(amount)));
            savingAccountDao.save(savingAccount);
            Date date = new Date();
            SavingTransaction savingTransaction = new SavingTransaction(date, "withdraw from saving account", "Account",
                    "Finished", amount, savingAccount.getAccountBalance(), savingAccount);
            transactionService.saveSavingWithDrawTransaction(savingTransaction);
        }
    }
}
