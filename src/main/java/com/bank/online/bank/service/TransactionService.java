package com.bank.online.bank.service;

import com.bank.online.bank.domain.PrimaryTransaction;
import com.bank.online.bank.domain.SavingTransaction;

import java.util.List;

public interface TransactionService {
    List<PrimaryTransaction> findPrimaryTransactionList(String username);

    List<SavingTransaction> findSAvingTransactionList(String username);

    void savePrimaryDepositTransaction(PrimaryTransaction primaryTransaction);

    void saveSavingDepositTransaction(SavingTransaction savingTransaction);

    void savePrimaryWithDrawTransaction(PrimaryTransaction primaryTransaction);

    void saveSavingWithDrawTransaction(SavingTransaction savingTransaction);
}
