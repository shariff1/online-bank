package com.bank.online.bank.service;

import com.bank.online.bank.dao.PrimaryAccountDao;
import com.bank.online.bank.dao.PrimaryTransactionDao;
import com.bank.online.bank.dao.SavingTransactionDao;
import com.bank.online.bank.domain.PrimaryTransaction;
import com.bank.online.bank.domain.SavingTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {
    @Autowired
    PrimaryAccountDao primaryAccountDao;
    @Autowired
    PrimaryTransactionDao primaryTransactionDao;
    @Autowired
    SavingTransactionDao savingTransactionDao;


    @Override
    public List<PrimaryTransaction> findPrimaryTransactionList(String username) {
        return null;
    }

    @Override
    public List<SavingTransaction> findSAvingTransactionList(String username) {
        return null;
    }

    @Override
    public void savePrimaryDepositTransaction(PrimaryTransaction primaryTransaction) {
        primaryTransactionDao.save(primaryTransaction);
    }

    @Override
    public void saveSavingDepositTransaction(SavingTransaction savingTransaction) {
        savingTransactionDao.save(savingTransaction);
    }

    @Override
    public void savePrimaryWithDrawTransaction(PrimaryTransaction primaryTransaction) {
        primaryTransactionDao.save(primaryTransaction);
    }

    @Override
    public void saveSavingWithDrawTransaction(SavingTransaction savingTransaction) {
        savingTransactionDao.save(savingTransaction);
    }
}
