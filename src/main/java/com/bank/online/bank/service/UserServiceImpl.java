package com.bank.online.bank.service;

import com.bank.online.bank.dao.RoleDao;
import com.bank.online.bank.dao.UserDao;
import com.bank.online.bank.domain.User;
import com.bank.online.bank.domain.UserRole;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(UserService.class);
    @Autowired
    UserDao userDao;
    @Autowired
    BCryptPasswordEncoder passwordEncoder;
    @Autowired
    RoleDao roleDao;
    @Autowired
    AccountService accountService;

    @Override
    public boolean checkUserExists(String username,
                                   String email) {
        //  return null != userDao.findByUsername(username) && null != userDao.findByEmail(email);
        if (checkUserNameExists(username) && checkEmailExists(email)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean checkEmailExists(String email) {
        if (findByEmail(email) != null) {
            return true;
        } else {
            return false;
        }
    }

    private User findByEmail(String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public boolean checkUserNameExists(String username) {
        if (findByUsername(username) != null) {
            return true;
        } else {
            return false;
        }
    }

    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }

    @Override
    public User createUser(User user,
                           Set<UserRole> userRole) {
        User foundUser = userDao.findByUsername(user.getUsername());
        if (foundUser != null) {
            LOG.info("User with username {} already exist. Nothing will be done. ", user.getUsername());
        } else {
            String encryptedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(encryptedPassword);
            for (UserRole ur : userRole
                    ) {
                roleDao.save(ur.getRole());
            }
            user.getUserRoles().addAll(userRole);
            user.setPrimaryAccount(accountService.createPrimaryAccount());
            user.setSavingAccount(accountService.createSavingAccount());
            foundUser = userDao.save(user);

        }
        return foundUser;
    }
}
