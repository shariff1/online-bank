package com.bank.online.bank.service;

import com.bank.online.bank.domain.PrimaryAccount;
import com.bank.online.bank.domain.SavingAccount;

import java.security.Principal;

public interface AccountService {
    PrimaryAccount createPrimaryAccount();

    SavingAccount createSavingAccount();

    void deposit(double amount,
                 String accountType,
                 Principal principal);

    void withdraw(String accountType,
                  double amount,
                  Principal principal);
}
